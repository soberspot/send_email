import yagmail, csv
import os
from configparser import ConfigParser

# Добавлен config.ini
# источник:   https://python-scripts.com/send-email-smtp-python

base_path = os.path.dirname(os.path.abspath(__file__))
config_path = os.path.join(base_path, "config.ini")
cfg = ConfigParser()
cfg.read(config_path)

from_addr = cfg.get("smtp", "from_addr")
password = cfg.get("smtp", "password")

yagmail.register(from_addr, password)

message = """
<h4>Уважаемый(ая) {name}</h4>  
your grade is {grade}
В связи с большим количеством обращений по внесению дополнений в повестку общего собрания членов
товарищества Правление СНТ Югтес сообщает об изменениии сроков проведения собрания.
"""

# receiver = 'soberspot@gmail.com'
body = '<h2>Уведомление</h2>' \
            '<h3>Правление СНТ № 2 АО "Югтекс" уведомляет об изменении даты проведения общего собрания</h3>'

filename = 'document.pdf'  # Прикрепляемый к рассылке файл (файлы)
html = '<a href="https://yugteks2.ru/sobranie">Общая информация по собранию</a>'
html1 = '<a href="https://yugteks2.ru/documents/docs">Документы к собранию</a>'
img = '/local/file/bunny.png'

yag = yagmail.SMTP("yugteks@gmail.com")
#  Отправка одного сообщения
# yag.send(to=receiver, subject="Тест Югмайл. Перенос даты", contents=[body, html, html1], attachments=[filename, filename])
# print('Письмо отправлено')

# Отправка рассылки сообщений адресатами из csv файла
print('Старт рассылки')

with open("contacts_file.csv") as file:
    reader = csv.reader(file)
    next(reader)  # Skip header row
    for name, email, grade in reader:
        yag.send(to=email, subject="Тест Югмайл адреса из CSV", contents=[body, message.format(name=name,grade=grade),html, html1], attachments=[filename, filename])

print('Рассылка отправлена')